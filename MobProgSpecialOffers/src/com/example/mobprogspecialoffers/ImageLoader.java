package com.example.mobprogspecialoffers;

import java.io.*;

import android.graphics.*;
import android.os.*;
import android.widget.*;

class ImageLoader extends AsyncTask<String, Void, Bitmap> {
	ImageView bmImage;

	public ImageLoader(ImageView bmImage) {
		this.bmImage = bmImage;
	}

	@Override
	protected Bitmap doInBackground(String... urls) {
		String urldisplay = urls[0];
		Bitmap mIcon11 = null;
		try {
			InputStream in = new java.net.URL(urldisplay).openStream();
			mIcon11 = BitmapFactory.decodeStream(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mIcon11;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		bmImage.setImageBitmap(result);
	}
}
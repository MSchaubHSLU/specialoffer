package com.example.mobprogspecialoffers;

import java.util.*;

import org.json.*;

import android.content.*;
import android.graphics.*;
import android.os.*;
import android.view.*;
import android.widget.*;

/**
 * A placeholder fragment containing a simple view.
 */
public class FavoritenFragment extends BaseFragment {
	int[] loadedfavoriten = null;
	static FavoritenFragment singelton;

	public static FavoritenFragment newInstance() {

		if (singelton == null) {
			FavoritenFragment fragment = new FavoritenFragment();
			Bundle args = new Bundle();
			fragment.setArguments(args);
			singelton = fragment;
		}
		return singelton;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		mainLayout = (LinearLayout) rootView.findViewById(R.id.fragmentlayout);
		return rootView;
	}

	public void loadOffers(JSONArray deals) {
		if (loadFavoriten()) {
			JSONArray favArray = new JSONArray();

			for (int j = 0; j < loadedfavoriten.length; j++) {
				for (int i = 0; i < deals.length(); i++) {
					try {
						JSONObject jo = deals.getJSONObject(i);
						if (jo.getInt("id") == loadedfavoriten[j]) {
							favArray.put(jo);
							break;
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			addOffer(favArray);
		} else {
			mainLayout = (LinearLayout) this.getView().findViewById(R.id.fragmentlayout);

			mainLayout.removeAllViews();
			TextView textView = new TextView(getActivity().getApplicationContext());
			textView.setTextColor(Color.parseColor("#000000"));
			textView.setText("Keine Favoriten konnten geladen werden");
			mainLayout.addView(textView);

		}
	}

	private boolean loadFavoriten() {
		try {

			SharedPreferences sharedPreferences = getActivity().getSharedPreferences("favoriten", Context.MODE_PRIVATE);
			sharedPreferences.contains("favoriten");
			Set<String> favoriten = sharedPreferences.getStringSet("favoriten", new HashSet<String>());

			if (favoriten.size() > 0) {

				List<String> favoritenArray = new ArrayList<String>(favoriten);

				loadedfavoriten = new int[favoriten.size()];
				for (int i = 0; i < favoriten.size(); i++) {
					loadedfavoriten[i] = Integer.parseInt(favoritenArray.get(i));
				}
				return true;
			}

		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
		return false;
	}

	// @Override
	// public void update(JSONArray deals) {
	// loadOffers(deals);
	// }

	@Override
	public void onResume() {
		super.onResume();
		if (getDeals().length() > 0) {
			loadOffers(getDeals());
		}

	}
}

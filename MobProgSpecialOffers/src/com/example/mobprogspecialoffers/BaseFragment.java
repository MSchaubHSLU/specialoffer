package com.example.mobprogspecialoffers;

import java.util.*;

import org.json.*;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;

/**
 * A placeholder fragment containing a simple view.
 */
public abstract class BaseFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	LinearLayout mainLayout;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */

	public BaseFragment() {
	}

	// publicabstract void update(JSONArray deals);
	protected void removeFromFav(int i) {

		final SharedPreferences preferences = getActivity().getSharedPreferences("favoriten", Context.MODE_PRIVATE);
		final Set<String> savedFavoriten = preferences.getStringSet("favoriten", new HashSet<String>());
		if (savedFavoriten.contains("" + i)) {
			savedFavoriten.remove("" + i);

			final SharedPreferences.Editor editor = preferences.edit();
			editor.clear();
			editor.putStringSet("favoriten", savedFavoriten);
			editor.apply();
		}
	}

	protected void addOffer(JSONArray jsonArray) {
		mainLayout.removeAllViews();
		for (int i = 0; i < jsonArray.length(); i++) {
			try {
				LayoutInflater inflater = LayoutInflater.from(getActivity());
				View itemContainer = inflater.inflate(R.layout.item_container, null);

				TextView titleText = (TextView) itemContainer.findViewById(R.id.title);
				TextView descriptionText = (TextView) itemContainer.findViewById(R.id.description);
				ImageView picture = (ImageView) itemContainer.findViewById(R.id.image);
				CheckBox togglButton = (CheckBox) itemContainer.findViewById(R.id.togglButton);

				titleText.setText(jsonArray.getJSONObject(i).getString("shop"));
				titleText.setTypeface(null, Typeface.BOLD);
				descriptionText.setText(jsonArray.getJSONObject(i).getString("offer"));

				if (jsonArray.getJSONObject(i).has("image") && jsonArray.getJSONObject(i).getString("image") != null) {
					new ImageLoader(picture).execute(jsonArray.getJSONObject(i).getString("image"));
				} else {
					picture.setImageResource(R.drawable.ic_launcher);
					picture.setContentDescription(titleText.getText());
				}

				togglButton.setId(jsonArray.getJSONObject(i).getInt("id"));
				togglButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						toggleFavorit((CheckBox) v);
						MainActivity main = (MainActivity) getActivity();
						// main.updateFavorites();
					}
				});
				if (isSaved(jsonArray.getJSONObject(i).getInt("id"))) {
					togglButton.setChecked(true);
				}

				mainLayout.addView(itemContainer);
				View v = new View(getActivity());
				v.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 5));
				v.setBackgroundColor(Color.parseColor("#B3B3B3"));
				mainLayout.addView(v);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	protected void saveToFav(int i) {

		final SharedPreferences preferences = getActivity().getSharedPreferences("favoriten", Context.MODE_PRIVATE);
		final Set<String> savedFavoriten = preferences.getStringSet("favoriten", new HashSet<String>());
		if (!savedFavoriten.contains("" + i)) {
			savedFavoriten.add("" + i);
			final SharedPreferences.Editor editor = preferences.edit();
			editor.clear();
			editor.putStringSet("favoriten", savedFavoriten);
			editor.apply();
		}
	}

	private void toggleFavorit(CheckBox cb) {

		if (cb.isChecked()) {
			saveToFav(cb.getId());
		} else {
			removeFromFav(cb.getId());
		}
		if (this instanceof FavoritenFragment) {
			this.onResume();
		}
	}

	private boolean isSaved(int i) {
		final SharedPreferences preferences = getActivity().getSharedPreferences("favoriten", Context.MODE_PRIVATE);
		final Set<String> savedFavoriten = preferences.getStringSet("favoriten", new HashSet<String>());
		return savedFavoriten.contains("" + i);
	}

	protected JSONArray getDeals() {
		JSONArray tmpJsonArray = ((MainActivity) getActivity()).getDeals();
		return tmpJsonArray;
	}
}

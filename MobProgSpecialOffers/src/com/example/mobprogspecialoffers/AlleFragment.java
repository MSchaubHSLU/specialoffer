package com.example.mobprogspecialoffers;

import org.json.*;

import android.os.*;
import android.view.*;
import android.widget.*;

/**
 * A placeholder fragment containing a simple view.
 */
public class AlleFragment extends BaseFragment {

	public static AlleFragment newInstance() {
		AlleFragment fragment = new AlleFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		mainLayout = (LinearLayout) rootView.findViewById(R.id.fragmentlayout);
		return rootView;
	}

	public void loadOffers(JSONArray deals) {
		addOffer(deals);
	}

	@Override
	public void onResume() {
		if (isAdded()) {
			super.onResume();
			loadOffers(getDeals());
		}
	}

}